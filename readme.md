### Setup Instructions
1. Clone this repo
2. Rename the file '.env.example' to '.env'
3. Turn on your php web server and create your database and set database credentials as defined the .env file
4. Open command prompt on the project's root directory
5. Run this command to setup the database: 'php artisan migrate:fresh --seed'
6. You database should now be migrated and seeded accordingly
7. Open the project url on your browser (this is dependent on the webserver software you are running)

### Dummy / Test Data
- Sample staffs and students are automatically created on the database for testing purposes

### Usage
- You can login as an admin using:
    -   Email: admin@mcuportal.app
    -   Password: 123456
- Note that password to all auto-generated test accounts is '123456'
- Upon a successful login, you will be greeted with your dashboard and a side-bar menu on the left
 