<?php

use Illuminate\Http\Response;

function validationErrorResponseForApi(\Illuminate\Validation\ValidationException $e){
    $flattened_errors = array_flatten($e->errors());
    $response['errors'] = $flattened_errors;
    $response['ok'] = false;
    $response['message'] = $e->getMessage();
    $status = Response::HTTP_UNAUTHORIZED;

    $response['rendered'] =  '<ul>' .
        implode('', array_map(function($error_item){
            return '<li>' . $error_item . '</li>';
        }, $flattened_errors))
        . '</ul>';

    return response($response, $status);
}

function listArrayInHtml(array $list){
    return '<ul>' .
        implode('', array_map(function($list_item){
            return '<li>' . $list_item . '</li>';
        }, $list))
        . '</ul>';
}

function greeting($name = ''){
    $hour = date('H');
    $greet_message = '';

    switch ($hour){
        case $hour <= 11 :
            $greet_message .= 'Good morning';
            break;
        case $hour <= 15:
            $greet_message .= 'Good afternoon';
            break;
        default:
            $greet_message .= 'Good evening';
            break;
    }

    return $greet_message . ( $name == '' ? '' : ', ' . $name);
}