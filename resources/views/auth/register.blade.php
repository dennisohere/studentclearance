@extends('layouts.site')

@section('site.content')
    <div class="row justify-content-center">
        <div class="col-lg-9 col-md-9">
            <div class="card bg-secondary shadow">
                <div class="card-header bg-white border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Student Registration</h3>
                        </div>
                        <div class="col-4 text-right">
                            {{--<a href="#!" class="btn btn-sm btn-primary">Settings</a>--}}
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" enctype="multipart/form-data" action="{{route('student.registration')}}">
                        @csrf
                        <h6 class="heading-small text-muted mb-4">Student information</h6>
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-first-name">Surname</label>
                                        <input type="text" id="input-first-name"
                                               name="surname"
                                               class="form-control form-control-alternative"
                                               placeholder="Surname" value="{{old('surname')}}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-last-name">Other names:</label>
                                        <input type="text" id="input-last-name"
                                               class="form-control form-control-alternative"
                                               name="other_names"
                                               placeholder="Other names" value="{{old('other_names')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-admission-year">Admission year:</label>
                                        <input type="text" id="input-admission-year"
                                               class="form-control form-control-alternative"
                                               name="admission_year"
                                               placeholder="Admission year:" value="{{old('admission_year')}}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-matric-no">Matric. No.</label>
                                        <input type="text" id="input-matric-no"
                                               class="form-control form-control-alternative"
                                               placeholder="Matriculation number:"
                                               name="matric_no" value="{{old('matric_no')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-password">Password:</label>
                                        <input type="password" id="input-password"
                                               class="form-control form-control-alternative"
                                               name="password"
                                               placeholder="">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-retype-password">Retype Password:</label>
                                        <input type="password" id="input-retype-password"
                                               class="form-control form-control-alternative"
                                               name="retype_password"
                                               placeholder="">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <hr class="my-4" />

                        <div class="row">
                            <div class="col-lg-7">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-department">Department:</label>
                                    <select class="form-control custom-select" name="department" id="input-department">
                                        @foreach(\App\Repositories\DepartmentRepository::init()->getAllDepartments() as $department)
                                        <option value="{{$department->id}}">{{$department->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-passport-photo">Upload passport photo:</label>
                                    <input type="file" id="input-passport-photo"
                                           class="form-control form-control-alternative"
                                           name="photo" accept="image/jpeg, image/png, image/jpg"
                                           placeholder="">
                                </div>
                            </div>
                        </div>

                        <hr class="my-4" />
                        <!-- Address -->
                        <h6 class="heading-small text-muted mb-4">Contact information</h6>
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-address">Home Address</label>
                                        <input id="input-address" class="form-control form-control-alternative"
                                               placeholder="Home Address"
                                               name="home_address"
                                               value="{{old('home_address')}}" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-phone">Phone:</label>
                                        <input type="text" id="input-phone"
                                               class="form-control form-control-alternative"
                                               placeholder="Phone:" value="{{old('phone')}}" name="phone">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">Email address</label>
                                        <input type="email" id="input-email"
                                               name="email"
                                               value="{{old('email')}}"
                                               class="form-control form-control-alternative" placeholder="">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <hr class="my-4" />

                        <h6 class="heading-small text-muted mb-4">Guardian information</h6>
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-parent-address">Contact Address</label>
                                        <input id="input-parent-address" class="form-control form-control-alternative"
                                               placeholder="Guardian Address"
                                               name="parent_address"
                                               value="{{old('parent_address')}}" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-parent-name">Guardian' name:</label>
                                        <input type="text" id="input-parent-name"
                                               class="form-control form-control-alternative"
                                               placeholder="Guardian's name:" value="{{old('parent_name')}}" name="parent_name">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <hr class="my-4" />

                        <div class="text-center">
                            <button type="submit" class="btn btn-primary my-4">Register</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
