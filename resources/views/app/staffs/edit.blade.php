@extends('layouts.base')

@php
    $pageTitle = $staff ? $staff->title : 'New Staff';
@endphp

@section('base')
    <div class="row justify-content-center">
        <div class="col-lg-9 col-md-9">
            <div class="card bg-secondary shadow">
                <div class="card-header bg-white border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Staff Registration</h3>
                        </div>
                        <div class="col-4 text-right">
                            {{--<a href="#!" class="btn btn-sm btn-primary">Settings</a>--}}
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" enctype="multipart/form-data" action="{{route('app.staffs.submit')}}">
                        @csrf
                        @if(isset($staff))
                            <input type="hidden" name="id" value="{{$staff->id}}">
                        @endif
                        <h6 class="heading-small text-muted mb-4">Staff information</h6>
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-first-name">Surname</label>
                                        <input type="text" id="input-first-name"
                                               name="surname"
                                               class="form-control form-control-alternative"
                                               placeholder="Surname" value="{{$staff->surname ?? old('surname')}}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-last-name">Other names:</label>
                                        <input type="text" id="input-last-name"
                                               class="form-control form-control-alternative"
                                               name="other_names"
                                               placeholder="Other names" value="{{$staff->other_names ?? old('other_names')}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-phone">Phone:</label>
                                        <input type="text" id="input-phone"
                                               class="form-control form-control-alternative"
                                               placeholder="Phone:" value="{{$staff->phone ?? old('phone')}}" name="phone">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-email">Email address</label>
                                        <input type="email" id="input-email"
                                               name="email"
                                               value="{{$staff->email ?? old('email')}}"
                                               class="form-control form-control-alternative" placeholder="Email:">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="my-4" />

                        <h6 class="heading-small text-muted mb-4">Account information</h6>

                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-username">Username:</label>
                                        <input type="text" id="input-username"
                                               class="form-control form-control-alternative"
                                               name="username"
                                               placeholder="Username">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-password">Password:</label>
                                        <input type="password" id="input-password"
                                               class="form-control form-control-alternative"
                                               name="password"
                                               placeholder="">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-retype-password">Retype Password:</label>
                                        <input type="password" id="input-retype-password"
                                               class="form-control form-control-alternative"
                                               name="retype_password"
                                               placeholder="">
                                    </div>
                                </div>

                            </div>
                        </div>

                        <hr class="my-4" />

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-department">Department:</label>
                                    <select class="form-control custom-select" name="department" id="input-department">
                                        <option value="">Pick a department</option>

                                    @foreach(\App\Repositories\DepartmentRepository::init()->getAllDepartments() as $department)
                                            <option value="{{$department->id}}">{{$department->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-department">Role:</label>
                                    <select class="form-control custom-select" name="role" id="input-department">
                                        <option>Pick a role</option>

                                        @foreach(\Spatie\Permission\Models\Role::whereNotIn('name',
                                        [config('system.roles.student'), config('system.roles.staff'), config('system.roles.admin')])->get() as $role)
                                            <option value="{{$role->id}}">{{$role->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            {{--<div class="col-lg-5">
                                <div class="form-group">
                                    <label class="form-control-label" for="input-passport-photo">Upload passport photo:</label>
                                    <input type="file" id="input-passport-photo"
                                           class="form-control form-control-alternative"
                                           name="photo" accept="image/jpeg, image/png, image/jpg"
                                           placeholder="">
                                </div>
                            </div>--}}
                        </div>

                        <hr class="my-4" />

                        <div class="text-center">
                            <button type="submit" class="btn btn-primary my-4">Submit</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('header') @overwrite
@section('footer') @overwrite