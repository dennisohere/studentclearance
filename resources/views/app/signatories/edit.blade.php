@extends('layouts.base')

@php
    $pageTitle = $signatory ? $signatory->title : 'New Office (Signatory)';
@endphp

@section('base')
    <div class="row justify-content-center">
        <div class="col-lg-9 col-md-9">
            <div class="card bg-secondary shadow">
                <div class="card-header bg-white border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Office (Signatory) Registration</h3>
                        </div>
                        <div class="col-4 text-right">
                            {{--<a href="#!" class="btn btn-sm btn-primary">Settings</a>--}}
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" enctype="multipart/form-data" action="{{route('app.signatories.submit')}}">
                        @csrf
                        @if(isset($signatory))
                            <input type="hidden" name="id" value="{{$signatory->id}}">
                        @endif
                        <h6 class="heading-small text-muted mb-4">Office (Signatory) information</h6>
                        <div class="pl-lg-4">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-first-name">Name</label>
                                        <input type="text" id="input-first-name"
                                               readonly
                                               class="form-control form-control-alternative"
                                               placeholder="Surname" value="{{$signatory->name ?? old('name')}}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-department">Staff:</label>
                                        <select class="form-control custom-select" name="staff" id="input-department">
                                            <option value="">Pick a staff</option>
                                            @foreach(\App\Repositories\StaffsRepository::init()->getAllStaffOfficers() as $staff)
                                                <option value="{{$staff->id}}"
                                                @if($signatory && $signatory->staff_id == $staff->id)
                                                    selected
                                                @elseif(old('staff') == $staff->id)
                                                    selected
                                                @endif
                                                >
                                                    {{$staff->getFullName()}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <hr class="my-4" />

                        <div class="text-center">
                            <button type="submit" class="btn btn-primary my-4">Submit</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('header') @overwrite
@section('footer') @overwrite