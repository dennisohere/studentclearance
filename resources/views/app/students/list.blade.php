@extends('layouts.app')

@php
$pageTitle = 'Students';
@endphp

@section('app.header')
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row">
                    <div class="col-lg-9 col-md-10">
                        <h1 class="display-2 text-white">
                            {{$pageTitle}}
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('app.content')
    <div class="row mt-1">
        <div class="col-xl-12 mb-5 mb-xl-0">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Students</h3>
                        </div>
                        <div class="col text-right">

                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <!-- Projects table -->
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Names</th>
                            <th scope="col">Email</th>
                            <th scope="col">Department</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($students as $student)
                        <tr>
                            <td>
                                {{$loop->index + 1}}
                            </td>
                            <th scope="row">
                                {{$student->getFullName()}}
                            </th>
                            <td>
                                {{$student->email}}
                            </td>
                            <td>
                                {{$student->department->name}}
                            </td>
                            <td>
                                <a href="{{route('app.students.preview', ['student' => $student->id])}}"
                                   onclick="var popup_window = window.open(this.href, 'mywin','location=0,toolbar=0,menubar=0,scrollbars=1,height=600,width=1000');
                           return false;"
                                   class="btn btn-sm btn-outline-info">
                                    View clearance
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    {!! $students->links() !!}
                </div>
            </div>
        </div>
    </div>
@endsection