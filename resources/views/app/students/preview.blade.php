@extends('layouts.base')

@php
    $pageTitle = $student->getFullName();
@endphp

@section('base')
    <div class="py-3 px-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="row">
                    <div class="col-2">
                        <div class="bg-light" style="width: 100px; height: 100px; background-image: url('/img/mcu_logo.jpeg'); background-size: cover; background-position: center top;"></div>
                    </div>
                    <div class="col-8">
                        <h3 class="display-4 text-center">McPHERSON UNIVERSITY</h3>
                        <h2 class="heading-title text-center">CLEARANCE FOR FINAL YEAR STUDENTS</h2>
                    </div>
                    <div class="col-2">
                        <div class="bg-light" style="width: 100px; height: 100px; background-image: url({{$student->getPhotoUrl()}}); background-size: cover; background-position: center top;"></div>
                    </div>
                </div>
            </div>
        </div>

        <hr class="my-4" />

        <div class="row">
            <div class="col-12">
                <h6 class="heading-section mb-4">Student information</h6>
                <div class="row">
                    <div class="col-12 my-2">
                        Names: <strong>{{$student->getFullName()}}</strong>
                    </div>
                    <div class="col-6 my-2">
                        Year of admission: <strong>{{$student->admission_year}}</strong>
                    </div>
                    <div class="col-6 my-2">
                        Matic No.: <strong>{{$student->matric_no}}</strong>
                    </div>
                    <div class="col-6 my-2">
                        Phone No.: <strong>{{$student->phone}}</strong>
                    </div>
                    <div class="col-6 my-2">
                        Email address.: <strong>{{$student->email}}</strong>
                    </div>
                    <div class="col-12 my-2">
                        Home address.: <strong>{{$student->home_address}}</strong>
                    </div>
                    <div class="col-6 my-2">
                        Parent's Name.: <strong>{{$student->parent_name}}</strong>
                    </div>
                    <div class="col-12 my-2">
                        Parent's contact address.: <strong>{{$student->parent_address}}</strong>
                    </div>
                </div>
            </div>
        </div>

        <hr class="my-4" />

        <div class="row">
            <div class="col-12">
                <h3 class="heading-section mb-4">Clearance</h3>
                @foreach($student->clearance_signatures as $clearance_signature)
                    <div class="mb-5">
                        <h2 class="heading-small">{{$clearance_signature->office->name}}</h2>
                        <div class="mb-3">
                            I certify that <strong>{{$student->getFullName()}}</strong>
                            of the Department of <strong>{{$student->department->name}}</strong> is not
                            indebted to ... in anyway. He / She is therefore cleared for graduation.
                        </div>
                        @if($clearance_signature->signed)
                        <div class="row">
                            <div class="col-6">
                                {{$clearance_signature->getStaffWhoSigned()}} <br>
                                {{$clearance_signature->office->office}}
                            </div>
                            <div class="col-3 offset-3">
                                <strong>Sign date:</strong> <br>
                                {{$clearance_signature->signed_on ? $clearance_signature->signed_on->format('jS F, Y') : ''}}
                            </div>
                        </div>
                        @else
                        <div class="badge-danger badge">
                            Not signed
                        </div>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('header') @overwrite
@section('footer') @overwrite