@push('scripts')
    @unless($errors->isEmpty())
        <script type="text/javascript">
            jQuery(document).ready(function () {
                Swal.fire({
                    title: '<strong>Error</strong>',
                    type: 'error',
                    html:'{!! listArrayInHtml($errors->all()) !!}',
                    showCloseButton: true,
                    // showCancelButton: true,
                    focusConfirm: false,
                })
            });
        </script>
    @endunless

    @include('flash::message')
@endpush

