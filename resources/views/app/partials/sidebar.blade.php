<!-- Sidenav -->
<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0" href="/">
            <img src="/img/mcu_logo.jpeg" class="navbar-brand-img" alt="..." style="height: 80px;
    max-height: none;">
        </a>

        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Navigation -->
            <h6 class="navbar-heading text-muted">
                Logged in as:
                {{implode(', ', auth()->user()->getRoleNames()->toArray())}}
            </h6>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="/">
                        <i class="ni ni-tv-2 text-primary"></i> Dashboard
                    </a>
                </li>
                @if(auth()->user()->hasRole(config('system.roles.admin')))
                <li class="nav-item">
                    <a class="nav-link" href="{{route('app.staffs.list')}}">
                        <i class="ni ni-circle-08 text-primary"></i> Staffs
                    </a>
                </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('app.students.list')}}">
                            <i class="ni ni-circle-08 text-primary"></i> Students
                        </a>
                    </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('app.signatories.list')}}">
                        <i class="ni ni-ruler-pencil text-primary"></i> Offices (Signatories)
                    </a>
                </li>
                @endif
                <li class="nav-item">
                    <a class="nav-link" href="/">
                        <i class="ni ni-tv-2 text-primary"></i> Change Password
                    </a>
                </li>
                <li class="nav-item ">
                    <!-- Divider -->
                    <hr class="my-3">
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/logout">
                        <i class="ni ni-key-25 text-info"></i> Logout
                    </a>
                </li>
            </ul>


        </div>
    </div>
</nav>