@extends('layouts.app')

@section('app.header')
    <!-- Header -->
    <div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" >
        <!-- Mask -->
        <span class="mask bg-gradient-default opacity-8"></span>
        <!-- Header container -->
        <div class="container-fluid d-flex align-items-center">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1 class="display-2 text-white">{{greeting(auth()->user()->person->surname)}}</h1>
                    <p class="text-white mt-0 mb-5">
                        Welcome to your account. Here you can see / sign available clearance forms.
                        @if(auth()->user()->person->department)
                            <br>
                        <span class="text-info">Department: {{auth()->user()->person->department->name}}</span>
                        @endif
                        @if(auth()->user()->person->isInRole(config('system.roles.officer')))
                            <br>
                            <span class="text-info">Office: {{auth()->user()->person->signatory_offices()->first()->office}}</span>
                        @endif
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('app.content')
    <div class="row mt-1">
        <div class="col-xl-12 mb-5 mb-xl-0">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Students for clearance</h3>
                        </div>
                        <div class="col text-right">

                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <!-- Projects table -->
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Student</th>
                            <th scope="col">Department</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(\App\Repositories\ClearanceRepository::init()->getAvailableClearanceForOfficer(auth()->user()->person) as $clearance)
                        <tr>
                            <td>
                                {{$loop->index + 1}}
                            </td>
                            <th scope="row">
                                {{$clearance->student->getFullName()}}
                            </th>
                            <td>
                                {{$clearance->student->department->name}}
                            </td>
                            <td>
                                <form action="{{route('app.clearance.sign')}}" class="form-inline d-inline-block" method="post">
                                    @csrf
                                    <input type="hidden" name="clearance_id" value="{{$clearance->id}}">

                                    @if(auth()->user()->person->isInRole(config('system.roles.dept_admin')))
                                        <input type="hidden" name="signatory_office_id"
                                               value="{{\App\Repositories\SignatoryRepository::init()->getSignatoryByName('Department')->id}}">
                                    @else
                                        <input type="hidden" name="signatory_office_id"
                                               value="{{auth()->user()->person->signatory_offices()->first()->id}}">
                                    @endif

                                    <input type="hidden" name="staff" value="{{auth()->user()->id}}">
                                    <button class="btn btn-sm btn-outline-warning" type="submit">Sign</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection