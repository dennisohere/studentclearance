@extends('layouts.app')

@section('app.header')
    <!-- Header -->
    <div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" >
        <!-- Mask -->
        <span class="mask bg-gradient-default opacity-8"></span>
        <!-- Header container -->
        <div class="container-fluid d-flex align-items-center">
            <div class="row">
                <div class="col-lg-9 col-md-10">
                    <h1 class="display-2 text-white">{{greeting(auth()->user()->person->surname)}}</h1>
                    <p class="text-white mt-0 mb-5">
                        Welcome to your account. You can see the progress made on your final year student clearance
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('app.content')
    <div class="row mt-1">
        <div class="col-xl-12 mb-5 mb-xl-0">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Clearance</h3>
                        </div>
                        <div class="col text-right">
                            <a href="{{route('app.students.preview', ['student' => auth()->user()->person->id])}}"
                               onclick="var popup_window = window.open(this.href, 'mywin','location=0,toolbar=0,menubar=0,scrollbars=1,height=600,width=1000');
                           return false;"
                               class="btn btn-sm btn-primary"><i class="fas fa-print"></i>
                            Preview / Print
                            </a>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Office</th>
                            <th scope="col">Signed On</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(auth()->user()->person->clearance_signatures as $clearance_signature)
                            <tr class="text-{{$clearance_signature->signed ? 'default' : 'warning'}}">
                                <td>
                                    {{$loop->index + 1}}
                                </td>
                                <th scope="row">
                                    {{$clearance_signature->office->name}}
                                </th>
                                <td>
                                    {{$clearance_signature->signed_on ? $clearance_signature->signed_on->format('jS F, Y') : ''}}
                                </td>
                                <td>
                                    @if($clearance_signature->signed)
                                    <i class="fas fa-check-circle fa-2x text-success"></i>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection