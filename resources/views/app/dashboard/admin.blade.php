@extends('layouts.app')

@section('app.header')
    <!-- Header -->
    <div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center"
         style="min-height: 600px; background-image: url(../assets/img/theme/profile-cover.jpg); background-size: cover; background-position: center top;">
        <!-- Mask -->
        <span class="mask bg-gradient-default opacity-8"></span>
        <!-- Header container -->
        <div class="container-fluid d-flex align-items-center">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h1 class="display-2 text-white">{{greeting(auth()->user()->person->surname)}}</h1>
                    <p class="text-white mt-0 mb-5">
                        Welcome to your account. As an Admin, you manage students, staffs and clearance data
                    </p>
                    {{--<a href="#!" class="btn btn-info">Edit profile</a>--}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('app.content')
    <!-- Card stats -->
    <div class="row">
        <div class="col-xl-3 col-lg-6">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">Registered Students</h5>
                            <span class="h2 font-weight-bold mb-0">
                                {{\App\Repositories\StudentsRepository::init()->getTotalStudents()}}
                            </span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                <i class="fas fa-users"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">Registered Staffs</h5>
                            <span class="h2 font-weight-bold mb-0">
                                {{\App\Repositories\StaffsRepository::init()->getTotalStaffs()}}
                            </span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                                <i class="fas fa-user-secret"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">Clearance Applications</h5>
                            <span class="h2 font-weight-bold mb-0">
                                {{\App\Repositories\ClearanceRepository::init()->getTotalClearance()}}
                            </span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
                                <i class="fas fa-file-archive"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">
                                Incomplete Clearance
                            </h5>
                            <span class="h2 font-weight-bold mb-0">
                                {{\App\Repositories\ClearanceRepository::init()->getTotalPendingClearance()}}
                            </span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                                <i class="fas fa-percent"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection