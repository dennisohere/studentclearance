<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>{{config('app.name')}}</title>

    <!-- Favicon -->
    <link href="/theme/assets/img/brand/favicon.png" rel="icon" type="image/png">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

    <!-- Icons -->
    <link href="/theme/assets/vendor/nucleo/css/nucleo.css" rel="stylesheet">
    <link href="/theme/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/lib/sweetalert/css/sweetalert2.min.css">
    <!-- Argon CSS -->
    <link type="text/css" href="/theme/assets/css/argon.min.css" rel="stylesheet">

    <style>
        .navbar-horizontal .navbar-brand img {
            height: 110px;
        }
    </style>

    @stack('styles')
</head>

<body class="">

@yield('base')

@include('app.partials.alerts')

<!-- Core -->
<script src="/theme/assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="/theme/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="/lib/sweetalert/js/sweetalert2.min.js"></script>
<!-- Argon JS -->
<script src="/theme/assets/js/argon.min.js"></script>

@stack('scripts')
</body>

</html>