@extends('layouts.base')

@section('base')

    @include('app.partials.sidebar')

    <!-- Main content -->
    <div class="main-content">
        @include('app.partials.topnav')
        <!-- Header -->
        @yield('app.header')

        <!-- Page content -->
        <div class="container-fluid mt--7">
            @yield('app.content')

            <!-- Footer -->
            <footer class="py-5">
                <div class="container">
                    <div class="row align-items-center justify-content-xl-between">
                        <div class="col-xl-6">
                            <div class="copyright text-center text-xl-left text-muted">
                                &copy; {{date('Y')}}
                                <a href="/" class="font-weight-bold ml-1">McU's {{config('app.name')}}</a>
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                                <li class="nav-item">
                                    <a href="#" class="nav-link">FAQ</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- Argon Scripts -->
        </div>
    </div>
    <!-- Argon Scripts -->
@endsection