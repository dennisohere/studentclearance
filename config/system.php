<?php

return [
    'roles' => [
        'student' => 'Student',
        'staff' => 'Staff',
        'admin' => 'Admin',
        'officer' => 'Officer',
        'dept_admin' => 'Dept. Admin',
    ],
    'departments' => [
        'comp_sci' => 'Computer Science',
        'mass_com' => 'Mass Communication',
        'bus_admin' => 'Business Administration',
    ],
    'default_password' => '123456',
];