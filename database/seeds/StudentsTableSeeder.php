<?php

use App\Student;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;


class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Student::truncate();

        /** @var Faker $faker */
        $faker = app(Faker::class);

        $dept_count = \App\Department::all()->count();

        $studentRepo = \App\Repositories\StudentsRepository::init();


        for ($i=1; $i <= 30; $i++){
            $studentRepo->createStudent([
                'surname' => $faker->lastName,
                'other_names' => $faker->firstName,
                'email' => 'student'. $i . '@mcuportal.app',
                'phone' => $faker->phoneNumber,
                'matric_no' => 'student'. $i,
                'admission_year' => $faker->year(),
                'home_address' => $faker->streetAddress,
                'parent_name' => $faker->firstName . ' ' . $faker->lastName,
                'parent_address' => $faker->address,
                'department_id' => $faker->numberBetween(1,$dept_count),
                'username' => 'student'. $i,
                'password' => config('system.default_password'),
            ]);
        }

    }
}
