<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement("SET foreign_key_checks=0");

        \App\User::truncate();

         $this->call(DepartmentTableSeeder::class);
         $this->call(RolesTableSeeder::class);
         $this->call(StaffsTableSeeder::class);
        $this->call(SignatoryTableSeeder::class);
        $this->call(StudentsTableSeeder::class);

        \DB::statement("SET foreign_key_checks=1");
    }
}
