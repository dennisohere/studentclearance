<?php

use App\Department;
use Illuminate\Database\Seeder;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::truncate();

        foreach (config('system.departments') as $key => $department){
            Department::create([
                'name' => $department
            ]);
        }
    }
}
