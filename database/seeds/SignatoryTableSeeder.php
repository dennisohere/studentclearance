<?php

use App\Signatory;
use Illuminate\Database\Seeder;

class SignatoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Signatory::truncate();

        Signatory::create([
           'name' => 'Department',
           'office' => 'Head of Department',
            'order' => 1
        ]);

        Signatory::create([
            'name' => 'Bursary',
            'office' => 'University Bursar',
            'order' => 2
        ]);

        Signatory::create([
            'name' => 'Library',
            'office' => 'University Librarian',
            'order' => 3
        ]);

        Signatory::create([
            'name' => 'Health Services',
            'office' => 'Director, Health Services',
            'order' => 4
        ]);

        Signatory::create([
            'name' => 'Chaplaincy Services',
            'office' => 'Director, Chaplaincy Services',
            'order' => 5
        ]);

        Signatory::create([
            'name' => 'Hall of Residence',
            'office' => 'Hall Manager',
            'order' => 6
        ]);

        Signatory::create([
            'name' => 'Student Affairs',
            'office' => 'Student Affairs Officer',
            'order' => 7
        ]);

        Signatory::create([
            'name' => 'Parent Consultative Forum',
            'office' => 'PCF Treasurer',
            'order' => 8
        ]);

        Signatory::create([
            'name' => 'Registry',
            'office' => 'Registrar',
            'order' => 9
        ]);
    }
}
