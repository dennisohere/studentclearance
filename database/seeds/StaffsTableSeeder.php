<?php

use App\Department;
use App\Staff;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

class StaffsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Staff::truncate();
        /** @var Faker $faker */
        $faker = app(Faker::class);

        $departments = Department::all();

        $staffRepo = \App\Repositories\StaffsRepository::init();

        $staffRepo->createStaff([
            'surname' => $faker->lastName,
            'other_names' => $faker->firstName,
            'email' => 'admin@mcuportal.app',
            'phone' => $faker->phoneNumber,
//                'department_id' => $department->id,
            'username' => 'admin',
            'password' => config('system.default_password'),
            'role' => \Spatie\Permission\Models\Role::findByName(config('system.roles.admin'))->id
        ]);

        /** @var Department $department */
        foreach ($departments as $department){
            $staffRepo->createStaff([
                'surname' => $faker->lastName,
                'other_names' => $faker->firstName,
                'email' => Str::slug($department->name) . '@mcuportal.app',
                'phone' => $faker->phoneNumber,
                'department_id' => $department->id,
                'username' => Str::slug($department->name),
                'password' => config('system.default_password'),
                'role' => \Spatie\Permission\Models\Role::findByName(config('system.roles.dept_admin'))->id
            ]);
        }

        for ($i=1; $i <= 5; $i++){
            $staffRepo->createStaff([
                'surname' => $faker->lastName,
                'other_names' => $faker->firstName,
                'email' => 'staff' . $i . '@example.com',
                'phone' => $faker->phoneNumber,
//                'department_id' => $department->id,
                'username' => 'staff' . $i,
                'password' => config('system.default_password'),
                'role' => \Spatie\Permission\Models\Role::findByName(config('system.roles.officer'))->id
            ]);
        }
    }
}
