<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->longText('metadata')->nullable();

            $table->timestamps();
        });

        Schema::table('staff', function (Blueprint $table) {
           $table->unsignedBigInteger('department_id')->nullable();
//           $table->foreign('department_id')->references('id')->on('departments');
        });

        Schema::table('students', function (Blueprint $table) {
            $table->unsignedBigInteger('department_id')->nullable();
            $table->foreign('department_id')->references('id')->on('departments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->dropForeign('students_department_id_foreign');
            $table->dropColumn('department_id');
        });

        Schema::table('staff', function (Blueprint $table) {
//            $table->dropForeign('staff_department_id_foreign');
            $table->dropColumn('department_id');
        });

        Schema::dropIfExists('departments');
    }
}
