<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('surname');
            $table->string('other_names');
            $table->integer('admission_year');
            $table->string('matric_no',15)->unique();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('home_address')->nullable();
            $table->string('parent_name')->nullable();
            $table->string('parent_address')->nullable();
            $table->string('photo_url')->nullable();

            $table->longText('metadata')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
