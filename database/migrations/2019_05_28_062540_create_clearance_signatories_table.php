<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClearanceSignatoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clearance_signatories', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('clearance_id');
            $table->foreign('clearance_id')->references('id')->on('clearances')->onDelete('cascade');

            $table->unsignedBigInteger('signatory_id');
            $table->foreign('signatory_id')->references('id')->on('signatories')->onDelete('cascade');

            $table->boolean('signed')->default(false);

            $table->unsignedBigInteger('signed_by')->nullable();
            $table->foreign('signed_by')->references('id')->on('staff')->onDelete('cascade');

            $table->dateTime('signed_on')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clearance_signatories');
    }
}
