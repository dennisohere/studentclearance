<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = 'staff';

    protected $guarded = ['id'];

    public function account()
    {
        return $this->morphOne(User::class,'person');
    }

    public function department()
    {
        return $this->belongsTo(Department::class,'department_id');
    }

    public function signatory_offices()
    {
        return $this->hasMany(Signatory::class,'staff_id');
    }

    public function getFullName()
    {
        return $this->surname . ' ' . $this->other_names;
    }

    public function isInDepartment($department)
    {
        return $this->department->name == $department;
    }

    public function isInRole($role)
    {
        return $this->account()->role($role)->exists();
    }
}
