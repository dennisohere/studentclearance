<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $guarded = ['id'];

    protected $table = 'departments';

    public function staffs()
    {
        return $this->hasMany(Staff::class,'department_id');
    }
}
