<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 5/27/2019
 * Time: 8:50 PM
 */

namespace App\Repositories;


use Spatie\Permission\Models\Role;

class RolesRepository
{
    /**
     * @var Role
     */
    private $role;


    /**
     * RolesRepository constructor.
     * @param Role $role
     */
    public function __construct(Role $role)
    {
        $this->role = $role;
    }
}