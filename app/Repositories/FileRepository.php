<?php

namespace App\Repositories;


use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class FileRepository
{

    public function saveFile(UploadedFile $file)
    {
        $media_type = $file->getClientMimeType();

        return $file->storePublicly('media/' . $media_type);
    }

    public function saveFileAs(UploadedFile $file, $saveAs)
    {
        $media_type = $file->getClientMimeType();

        return $file->storePubliclyAs('media/' . $media_type, $saveAs);
    }

    /**
     * @return self
     */
    public static function init()
    {
        return app(self::class);
    }

    public function deleteFile($location)
    {
        Storage::disk('local')->delete($location);
    }

}