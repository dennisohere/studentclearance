<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 5/26/2019
 * Time: 3:04 PM
 */

namespace App\Repositories;


use App\Signatory;

class SignatoryRepository
{
    /**
     * @var Signatory
     */
    private $signatory;


    /**
     * SignatorysRepository constructor.
     * @param Signatory $signatory
     */
    public function __construct(Signatory $signatory)
    {
        $this->signatory = $signatory;
    }

    /**
     * @return self
     */
    public static function init()
    {
        return app(self::class);
    }

    public function getAllSignatories()
    {
        return $this->signatory->orderBy('order', 'asc')->latest()->get();
    }

    public function getSignatoryByName($name)
    {
        return $this->signatory->whereName($name)->first();
    }
}