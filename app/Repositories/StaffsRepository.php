<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 5/26/2019
 * Time: 3:04 PM
 */

namespace App\Repositories;


use App\Staff;
use Illuminate\Database\Query\Builder;
use Spatie\Permission\Models\Role;

class StaffsRepository
{
    /**
     * @var Staff
     */
    private $staff;


    /**
     * StaffsRepository constructor.
     * @param Staff $staff
     */
    public function __construct(Staff $staff)
    {
        $this->staff = $staff;
    }

    /**
     * @param array $payload
     * @return Staff
     */
    public function createStaff(array $payload)
    {
        $staff = $this->staff->create([
            'surname' => $payload['surname'],
            'other_names' => $payload['other_names'],
            'email' => $payload['email'],
            'phone' => $payload['phone'],
            'department_id' => $payload['department_id'] ?? null
        ]);

        $staff_role = Role::findByName(config('system.roles.staff'));

        // todo: create staff login account
        UsersRepository::init()->createUser($staff, [
            'email' => $payload['email'],
            'username' => $payload['username'],
            'password' => $payload['password']
        ], [$staff_role->id,$payload['role']]);

        return $staff;
    }

    /**
     * @return self
     */
    public static function init()
    {
        return app(self::class);
    }

    public function getTotalStaffs()
    {
        return $this->staff->count();
    }

    public function getAllStaffs()
    {
        return $this->staff->latest()->orderBy('surname', 'asc')->get();
    }

    public function getAllStaffOfficers()
    {
        return $this->getAllStaffsWithRole(config('system.roles.officer'));
    }

    public function getAllStaffsWithRole($role)
    {
        /** @var Builder $query */
        $query = $this->staff->latest();

        return $query->whereHas('account', function($account_query) use ($role){
            return $account_query->role($role);
        })->has('signatory_offices', '==', 0)
            ->get();
    }
}