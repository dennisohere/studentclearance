<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 5/28/2019
 * Time: 6:32 AM
 */

namespace App\Repositories;


use App\Clearance;
use App\Repositories\SignatoryRepository;
use App\Staff;
use App\Student;
use Illuminate\Database\Eloquent\Builder;

class ClearanceRepository
{
    /**
     * @var Clearance
     */
    private $clearance;


    /**
     * ClearanceRepository constructor.
     * @param Clearance $clearance
     */
    public function __construct(Clearance $clearance)
    {
        $this->clearance = $clearance;
    }

    /**
     * @param $id
     * @return Clearance
     */
    public function getClearanceById($id)
    {
        return $this->clearance->find($id);
    }

    public function setupStudentClearanceForm(Student $student)
    {
        // todo: save clearance form
        /** @var Clearance $clearance */
        $clearance = $this->clearance->create([
            'student_id' => $student->id
        ]);

        // todo: add signatories / offices to sign
        foreach (SignatoryRepository::init()->getAllSignatories() as $signatory){
            $clearance->signatories()->create([
                'signatory_id' => $signatory->id
            ]);
        }
    }

    public function getTotalClearance()
    {
        return $this->clearance->latest()->count();
    }

    public function getTotalPendingClearance()
    {
        return $this->clearance->where('completed', false)->count();
    }

    /**
     * @return self
     */
    public static function init()
    {
        return app(self::class);
    }

    public function signStudentClearance($signatory_office_id, $clearance_id)
    {
        $clearance = $this->getClearanceById($clearance_id);

        $clearance_signatory = $clearance->signatories()->where('signatory_id', $signatory_office_id)->first();

        $clearance_signatory->fill([
            'signed' => true,
            'signed_by' => auth()->user()->id,
            'signed_on' => date('Y-m-d H:i:s')
        ]);

        $clearance_signatory->save();

        if($clearance->hasSignedAllSignatories()){
            $clearance->update([
               'completed' => true,
               'completed_at' => date('Y-m-d H:i:s')
            ]);
        }
    }

    public function getAvailableClearanceForOfficer(Staff $staff_officer)
    {
        $clearance = collect();

        if($staff_officer->isInRole(config('system.roles.dept_admin'))){
            // todo: clearance would include students in staffs department
            $result_dept = $this->clearance->whereHas('student', function($student_query) use ($staff_officer){
                return $student_query->where('department_id', $staff_officer->department_id)
                    ->whereHas('clearance_signatures', function ($clearance_signatures_query){
                        return $clearance_signatures_query
                            ->where('signed', false)
                            ->where('signatory_id', SignatoryRepository::init()->getSignatoryByName('Department')->id)
                            ;
                    })
                    ;
            })
                ->oldest()->with('student')
                ->where('completed', false)->get();

            foreach ($result_dept as $item){
                $clearance->push($item);
            }
        }

        if($staff_officer->isInRole(config('system.roles.officer'))){
            // todo: get all the offices on which this staff is heading
            $signatory_id = $staff_officer->signatory_offices()->first()->id;

            $result_officer = $this->clearance->oldest()
                ->whereHas('signatories', function($signatory_query) use ($signatory_id){
                    /** @var Builder $signatory_query */
                    return $signatory_query
                    ->where('signatory_id', $signatory_id)
                    ->where('signed', false)
                    ;
            })
                ->whereHas('signatories', function($signatory_query) use ($signatory_id){ // ensure previous signatory has signed
                    /** @var Builder $signatory_query */
                    return $signatory_query
                        ->where('signatory_id', $signatory_id - 1)
                        ->where('signed', true)
                        ;
                })
                ->with('student')->get();

            foreach ($result_officer as $item){
                $clearance->push($item);
            }


        }

       return $clearance;
    }

}