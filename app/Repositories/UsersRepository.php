<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 5/26/2019
 * Time: 5:34 PM
 */

namespace App\Repositories;


use App\User;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UsersRepository
{
    /**
     * @var User
     */
    private $user;


    /**
     * UsersRepository constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }


    /**
     * @param $person
     * @param array $payload
     * @param $role
     * @return User
     */
    public function createUser($person, array $payload, $role)
    {
        /** @var User $new_user */
        $new_user = $this->user->newInstance([
            'username' => $payload['username'],
            'email' => $payload['email'],
            'password' => Hash::make($payload['password']),
        ]);

        $person->account()->save($new_user);

        $new_user->assignRole($role);

        return $new_user;
    }

    /**
     * @return self
     */
    public static function init()
    {
        return app(self::class);
    }
}