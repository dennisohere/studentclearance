<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 5/26/2019
 * Time: 3:04 PM
 */

namespace App\Repositories;


use App\Student;
use Spatie\Permission\Models\Role;

class StudentsRepository
{
    /**
     * @var Student
     */
    private $student;


    /**
     * StudentsRepository constructor.
     * @param Student $student
     */
    public function __construct(Student $student)
    {
        $this->student = $student;
    }

    /**
     * @param array $payload
     * @return Student
     */
    public function createStudent(array $payload)
    {
//        dd($payload['username']);

        $student = $this->student->create([
            'surname' => $payload['surname'],
            'other_names' => $payload['other_names'],
            'email' => $payload['email'],
            'phone' => $payload['phone'],
            'matric_no' => $payload['matric_no'],
            'admission_year' => $payload['admission_year'],
            'home_address' => $payload['home_address'],
            'parent_name' => $payload['parent_name'],
            'parent_address' => $payload['parent_address'],
            'department_id' => $payload['department_id']
        ]);

        $student_role = Role::findByName(config('system.roles.student'));

        // todo: create student login account
        UsersRepository::init()->createUser($student, [
            'email' => $payload['email'],
            'username' => $payload['username'],
            'password' => $payload['password']
        ], [$student_role->id]);

        // todo: setup student clearance form

        ClearanceRepository::init()->setupStudentClearanceForm($student);

        return $student;
    }

    /**
     * @return self
     */
    public static function init()
    {
        return app(self::class);
    }

    public function getTotalStudents()
    {
        return $this->student->count();
    }

    public function getStudents()
    {
        return $this->student->orderBy('surname', 'asc')->oldest()->paginate(20);
    }
}