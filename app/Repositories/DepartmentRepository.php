<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 5/27/2019
 * Time: 5:31 PM
 */

namespace App\Repositories;


use App\Department;

class DepartmentRepository
{
    /**
     * @var Department
     */
    private $department;


    /**
     * DepartmentRepository constructor.
     * @param Department $department
     */
    public function __construct(Department $department)
    {
        $this->department = $department;
    }

    public function getAllDepartments()
    {
        return $this->department->all();
    }

    /**
     * @return self
     */
    public static function init()
    {
        return app(self::class);
    }
}