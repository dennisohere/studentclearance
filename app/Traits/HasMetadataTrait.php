<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 5/26/2019
 * Time: 2:35 PM
 */

namespace App\Traits;


trait HasMetadataTrait
{
    public function getMetadataAttribute($value)
    {
        if(!$value) return [];
        return json_decode($value, true);
    }

}