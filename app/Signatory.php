<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Signatory extends Model
{
    protected $guarded = ['id'];

    protected $table = 'signatories';

    public function staff()
    {
        return $this->belongsTo(Staff::class);
    }
}
