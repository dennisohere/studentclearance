<?php

namespace App\Http\Controllers;

use App\Repositories\ClearanceRepository;
use Illuminate\Http\Request;

class ClearanceController extends Controller
{
    public function signClearanceForm()
    {
        $data = \request()->all();

        ClearanceRepository::init()->signStudentClearance($data['signatory_office_id'],$data['clearance_id']);

        flash()->info('Clearance has been signed');

        return redirect()->back();
    }
}
