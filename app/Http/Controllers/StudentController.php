<?php

namespace App\Http\Controllers;

use App\Repositories\FileRepository;
use App\Repositories\StudentsRepository;
use App\Student;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class StudentController extends Controller
{
    use RegistersUsers;


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'same:retype_password'],
            'surname' => ['required', 'string'],
            'other_names' => ['required', 'string'],
            'photo' => ['required', 'image'],
            'phone' => ['required', 'string'],
            'matric_no' => ['required', 'string'],
            'admission_year' => ['required'],
            'home_address' => ['required', 'string'],
            'parent_name' => ['required', 'string'],
            'parent_address' => ['required', 'string'],
            'department' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Student
     */
    protected function create(array $data)
    {
        $studentRepo = StudentsRepository::init();

        $student = $studentRepo->createStudent([
            'surname' => $data['surname'],
            'other_names' => $data['other_names'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'matric_no' => $data['matric_no'],
            'admission_year' => $data['admission_year'],
            'home_address' => $data['home_address'],
            'parent_name' => $data['parent_name'],
            'parent_address' => $data['parent_address'],
            'department_id' => $data['department'],
            'username' => $data['matric_no'],
            'password' => $data['password'],
        ]);

        // todo: save student photo
        $uploaded_image = $data['photo'];
        $filename = time() . '.' . $uploaded_image->clientExtension();
        $path = FileRepository::init()->saveFileAs($uploaded_image, $filename);
        $student->fill([
           'photo_url' => $path
        ]);
        $student->save();

        return $student;
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $user = $this->create($request->all());

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    /**
     * The student has been registered.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Student $student
     * @return mixed
     */
    protected function registered(Request $request, Student $student)
    {

        flash()->success($student->getFullName() . ' has been successfully registered');

        return redirect()->to('/login');
    }

    public function listStudentsPage()
    {
        $students = StudentsRepository::init()->getStudents();

        return view('app.students.list', compact('students'));
    }

    public function previewStudentClearance(Student $student)
    {
        return view('app.students.preview', compact('student'));
    }
}
