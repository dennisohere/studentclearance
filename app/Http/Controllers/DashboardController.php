<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function dashboard()
    {
        /** @var User $user */
        $user = auth()->user();

        $dashboard_view_file = 'student';

        if($user->hasRole(config('system.roles.staff'))) $dashboard_view_file = 'staff';
        if($user->hasRole(config('system.roles.admin'))) $dashboard_view_file = 'admin';

        return view('app.dashboard.' . $dashboard_view_file);
    }
}
