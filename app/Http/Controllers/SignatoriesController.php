<?php

namespace App\Http\Controllers;

use App\Repositories\SignatoryRepository;
use App\Signatory;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class SignatoriesController extends Controller
{
    public function listStaffsPage()
    {
        $signatories = SignatoryRepository::init()->getAllSignatories();

        return view('app.signatories.list', compact('signatories'));
    }

    public function saveSignatoryPage(Signatory $signatory = null)
    {
        return view('app.signatories.edit', compact('signatory'));
    }

    public function saveSignatory()
    {
        $data = \request()->all();

        $signatory = Signatory::find($data['id']);

        $signatory->fill([
            'staff_id' => $data['staff']
        ]);

        $signatory->save();

        if($signatory->staff){
            flash()->success($signatory->staff->getFullName() . ' has been assigned to ' . $signatory->name);
        } else {
            flash()->success($signatory->name . ' has been updated');
        }

        return redirect()->back();
    }
}
