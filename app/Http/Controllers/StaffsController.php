<?php

namespace App\Http\Controllers;

use App\Repositories\StaffsRepository;
use App\Staff;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StaffsController extends Controller
{
    use RegistersUsers;

    public function listStaffsPage()
    {
        $staffs = StaffsRepository::init()->getAllStaffs();

        return view('app.staffs.list', compact('staffs'));
    }

    public function deleteStaff(Staff $staff)
    {
        try {
            $staff->delete();

            flash()->info('Deleted!');

            return redirect()->back();

        } catch (\Exception $e) {

            flash()->warning($e->getMessage());

            return redirect()->back();
        }
    }

    public function saveStaffPage(Staff $staff = null)
    {
        return view('app.staffs.edit', compact('staff'));
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'same:retype_password'],
            'surname' => ['required', 'string'],
            'other_names' => ['required', 'string'],
//            'photo' => ['required', 'image'],
            'phone' => ['required', 'string'],
            'department' => ['sometimes'],
            'role' => ['sometimes'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Staff
     */
    protected function create(array $data)
    {
        $staffRepo = StaffsRepository::init();

        $staff = $staffRepo->createStaff([
            'surname' => $data['surname'],
            'other_names' => $data['other_names'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'department_id' => $data['department'],
            'username' => $data['username'],
            'password' => $data['password'],
            'role' => $data['role'],
        ]);

        /*// todo: save student photo
        $uploaded_image = $data['photo'];
        $filename = time() . '.' . $uploaded_image->clientExtension();
        $path = FileRepository::init()->saveFileAs($uploaded_image, $filename);
        $student->fill([
            'photo_url' => $path
        ]);
        $student->save();*/

        return $staff;
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $user = $this->create($request->all());

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    /**
     * The student has been registered.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Staff $staff
     * @return mixed
     */
    protected function registered(Request $request, Staff $staff)
    {
        flash()->success($staff->getFullName() . ' has been successfully saved');

        return redirect()->back();
    }
}
