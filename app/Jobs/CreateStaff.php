<?php

namespace App\Jobs;

use App\Staff;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CreateStaff implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var array
     */
    private $payload;

    /**
     * Create a new job instance.
     *
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        //
        $this->payload = $payload;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $staff_account = User::create([
           'username' => $this->payload['username'],
           'password' => bcrypt(config('system.default_password'))
        ]);

        /** @var Staff $staff */
        $staff = Staff::create([
            'first_name' => $this->payload['first_name'],
            'last_name' => $this->payload['last_name'],
        ]);


    }
}
