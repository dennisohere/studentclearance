<?php

namespace App;

use App\Traits\HasMetadataTrait;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasMetadataTrait;

    protected $guarded = ['id'];

    protected $table = 'students';

    public function account()
    {
        return $this->morphOne(User::class,'person');
    }

    public function clearance()
    {
        return $this->hasOne(Clearance::class,'student_id');
    }

    public function clearance_signatures()
    {
        return $this->hasManyThrough(ClearanceSignatory::class,Clearance::class,'student_id','clearance_id');
    }

    public function department()
    {
        return $this->belongsTo(Department::class,'department_id');
    }

    public function getFullName()
    {
        return $this->surname . ' ' . $this->other_names;
    }

    public function getPhotoUrl()
    {
        return url('/uploads/' . $this->photo_url);
    }

}
