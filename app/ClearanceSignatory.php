<?php

namespace App;

use App\Repositories\StaffsRepository;
use Illuminate\Database\Eloquent\Model;

class ClearanceSignatory extends Model
{
    protected $table = 'clearance_signatories';

    protected $guarded = ['id'];

    protected $dates = [
        'signed_on'
    ];

    protected $casts = [
        'signed' => 'boolean'
    ];

    public function office()
    {
        return $this->belongsTo(Signatory::class,'signatory_id');
    }

    public function signedByStaff()
    {
        return $this->belongsTo(Staff::class,'signed_by');
    }

    public function clearance()
    {
        return $this->belongsTo(Clearance::class,'clearance_id');
    }

    public function getStaffWhoSigned()
    {
        return $this->signedByStaff->getFullName();
    }
}
