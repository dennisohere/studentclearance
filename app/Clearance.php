<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clearance extends Model
{
    protected $table = 'clearances';

    protected $guarded = ['id'];

    protected $dates = [
        'completed_at'
    ];

    protected $casts = [
        'completed' => 'boolean'
    ];

    public function signatories()
    {
        return $this->hasMany(ClearanceSignatory::class, 'clearance_id');
    }

    public function student()
    {
        return $this->belongsTo(Student::class,'student_id');
    }

    public function totalSignedSignatories()
    {
        return $this->signatories()->where('signed', true)->count();
    }

    public function totalSignatories()
    {
        return $this->signatories()->count();
    }

    public function hasSignedAllSignatories()
    {
        return $this->signatories()->where('signed', false)->count() == 0;
    }
}

