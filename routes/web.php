<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();


Route::name('app.')->middleware('auth')->group(function(){
    Route::get('/', 'DashboardController@dashboard')->name('dashboard');
    Route::get('/dashboard', 'DashboardController@dashboard')->name('dashboard');

    Route::name('staffs.')->prefix('staffs')->group(function(){
       Route::get('/', 'StaffsController@listStaffsPage')->name('list');

       Route::get('save/{staff?}', 'StaffsController@saveStaffPage')->name('save');
       Route::post('submit', 'StaffsController@register')->name('submit');
       Route::get('{staff}/delete', 'StaffsController@deleteStaff')->name('delete');
    });

    Route::name('students.')->prefix('students')->group(function(){
        Route::get('/', 'StudentController@listStudentsPage')->name('list');
        Route::get('{student}/preview-clearance', 'StudentController@previewStudentClearance')->name('preview');
    });

    Route::name('signatories.')->prefix('signatories')->group(function(){
        Route::get('/', 'SignatoriesController@listStaffsPage')->name('list');
        Route::get('save/{signatory?}', 'SignatoriesController@saveSignatoryPage')->name('save');
        Route::post('submit', 'SignatoriesController@saveSignatory')->name('submit');

    });

    Route::name('clearance.')->prefix('clearance')->group(function(){
        Route::post('sign', 'ClearanceController@signClearanceForm')->name('sign');

    });
});



Route::post('students/registration', 'StudentController@register')->name('student.registration');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
